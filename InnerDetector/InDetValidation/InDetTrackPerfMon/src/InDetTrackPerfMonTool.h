/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef INDETTRACKPERFMON_INDETTRACKPERFMONTOOL_H
#define INDETTRACKPERFMON_INDETTRACKPERFMONTOOL_H

/**
 * @file InDetTrackPerfMonTool.h
 * header file for class of same name
 * @author marco aparo
 * @date 16 February 2023
**/

/// gaudi includes
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/Service.h"

/// Athena includes
#include "AthenaMonitoring/ManagedMonitorToolBase.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

/// EDM includes
#include "xAODCore/BaseContainer.h"
#include "xAODCore/AuxContainerBase.h"

/// local includes
#include "InDetTrackPerfMon/ITrackAnalysisDefinitionSvc.h"
#include "TrackAnalysisCollections.h"
#include "RoiSelectionTool.h"
#include "InDetTrackPerfMon/ITrackSelectionTool.h"
#include "ITrackMatchingTool.h"
#include "TrackAnalysisPlotsMgr.h"
#include "TrackAnalysisInfoWriteTool.h"

/// STL includes
#include <string>
#include <vector>


class InDetTrackPerfMonTool : public ManagedMonitorToolBase {

public :

    /// Constructor with parameters
    InDetTrackPerfMonTool( const std::string& type, const std::string& name, const IInterface* parent );

    /// Destructor
    virtual ~InDetTrackPerfMonTool();

    virtual StatusCode initialize();
    virtual StatusCode bookHistograms();
    virtual StatusCode fillHistograms();
    virtual StatusCode procHistograms();

private :

    /// prevent default construction
    InDetTrackPerfMonTool();

    /// retrieve all collections and load them into trkAnaCollections object
    StatusCode loadCollections( IDTPM::TrackAnalysisCollections& trkAnaColls );

    /// Offline TrackParticleContainer's name
    SG::ReadHandleKey< xAOD::TrackParticleContainer > m_offlineTrkParticleName {
        this, "OfflineTrkParticleContainerName", "InDetTrackParticles", "Name of container of offline tracks" };

    /// Trigger TrackParticleContainer's name
    SG::ReadHandleKey< xAOD::TrackParticleContainer > m_triggerTrkParticleName {
        this, "TriggerTrkParticleContainerName", "HLT_IDTrack_Electron_IDTrig", "Name of container of trigger tracks" };

    /// TruthParticle container's name
    SG::ReadHandleKey< xAOD::TruthParticleContainer > m_truthParticleName {
        this, "TruthParticleContainerName",  "TruthParticles", "Name of container of TruthParticles" };

    /// TruthEvent container's name
    SG::ReadHandleKey< xAOD::TruthEventContainer > m_truthEventName {
        this, "TruthEvents", "TruthEvents", "Name of the truth events container probably either TruthEvent or TruthEvents" };

    /// TruthPileupEvent container's name
    SG::ReadHandleKey< xAOD::TruthPileupEventContainer > m_truthPileUpEventName {
        this, "TruthPileupEvents", "TruthPileupEvents", "Name of the truth pileup events container probably TruthPileupEvent(s)" };

    /// EventInfo container name
    SG::ReadHandleKey< xAOD::EventInfo > m_eventInfoContainerName {
        this, "EventInfoContainerName", "EventInfo", "event info" };

    /// WriteHandle for trkAnaInfo for reprocessing
    SG::WriteHandleKey< xAOD::BaseContainer > m_trkAnaInfoKey {
        this, "TrkAnaInfoKey", "TrackAnalysisInfo", "Dedicated TrackAnalysis Info written out" };

    /// -----------------------------
    /// --------- Sub-Tools ---------
    /// -----------------------------

    PublicToolHandle< Trig::TrigDecisionTool > m_trigDecTool {
        this, "TrigDecisionTool", "Trig::TrigDecisionTool/TrigDecisionTool", "" };

    ToolHandle< IDTPM::ITrackSelectionTool > m_trackQualitySelectionTool {
        this, "TrackQualitySelectionTool", "IDTPM::InDetTrackPerfMon/ITrackSelectionTool", "Wrapper-tool to perform general quality-based track(truth) selection" };

    ToolHandle< IDTPM::RoiSelectionTool > m_roiSelectionTool {
        this, "RoiSelectionTool", "IDTPM::InDetTrackPerfMon/RoiSelectionTool", "Tool to retrieve and select RoIs" };

    ToolHandle< IDTPM::ITrackSelectionTool > m_trackRoiSelectionTool {
        this, "TrackRoiSelectionTool", "IDTPM::InDetTrackPerfMon/ITrackSelectionTool", "Tool to select track within a RoI" };

    ToolHandle< IDTPM::ITrackMatchingTool > m_trackMatchingTool {
        this, "TrackMatchingTool", "IDTPM::InDetTrackPerfMon/ITrackMatchingTool", "Tool to match test to reference tracks and viceversa" };

    ToolHandle< IDTPM::TrackAnalysisInfoWriteTool > m_trkAnaInfoWriteTool {
        this, "TrackAnalysisInfoWriteTool", "IDTPM::InDetTrackPerfMon/TrackAnalysisInfoWriteTool", "Tool to write TrackAnalysisInfo to StoreGate" };

    StringProperty m_anaTag{ this, "AnaTag", "", "Track analysis tag" }; 

    BooleanProperty m_doMatch{ this, "doMatch", false, "Enable TrackMatchingTool" };

    BooleanProperty m_writeOut{ this, "writeOut", false, "Write TrkAnaInfo Collection to AOD_IDTPM" };

    /// TrackAnalysisDefinitionSvc
    SmartIF<ITrackAnalysisDefinitionSvc> m_trkAnaDefSvc;

    /// plots
    std::vector< std::unique_ptr< IDTPM::TrackAnalysisPlotsMgr > >  m_trkAnaPlotsMgrVec;
};

#endif
